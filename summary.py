from Bio import SeqIO
from Bio.Alphabet import IUPAC
import os, yaml,sys

handle = open("abalone.xml", "rU")

giande = open("giande.txt", "w")

numberofhitsbelowe = 0
numberwithgoterms = 0
numberofheat = 0



#print ('Type maximum e value')
#e = float(input())

e = sys.argv[1]

database_files = os.listdir("annotated_db_outputs")
database_paths = []


for dbf in database_files:
	if ( dbf.endswith("gb")):
		database_paths.append("annotated_db_outputs/%s" % dbf)



database = SeqIO.index_db(":memory:",database_paths,"gb")

for record_id in database:

	record = database[record_id]

	record_annotations = yaml.load(record.annotations['comment'])

	best_gi = record_annotations['GI']
	evalue = record_annotations['E Value']
	giande.write(' %s\t%s\n' % (best_gi, evalue))


	if record_annotations['E Value'] <= e:
		numberofhitsbelowe = numberofhitsbelowe +1
		if record_annotations['GO'] != '':
		
			numberwithgoterms = numberwithgoterms +1
			if "heat" in record_annotations['GO'] or "temperature" in record_annotations['GO']:
				print best_gi
				print record_annotations['GO']
				numberofheat = numberofheat +1






print 'Number of hits: \t%i\nNumber of hits with go terms: \t%i\nNumber with heat\t%i\n' % (numberofhitsbelowe, numberwithgoterms, numberofheat)




giande.close()
handle.close()	





	# if records.annotations['E Value'] == '2.37949e-25':
	# 	print records.annotations['E Value']
	# 	print records.id
	# 	print records.description
	# 	print records.annotations

