from Bio import Blast
from Bio.Blast import NCBIXML


f_in = open('./first100.xml', 'r')
f_out = open('./2.txt', 'w')

parser = Blast.NCBIXML.parse(f_in)

def best_hit(hits):

	minimum = 10000
	i_min = 0
	
	for i in range(0,(len(hits)-1)):
		current_e = hits[i].e	
		if current_e < minimum:
			minimum = current_e
			i_min = i
			print "E value:%g" % minimum
			print entry.query_id

	return hits[i_min]

row = "Hit:\t Hit ID:\t, E value:\t, Query ID:\t, Length:\t\n"  
f_out.write(row)

for entry in parser:
	if ( len(entry.descriptions) > 0 ):
		
		besthit = best_hit(entry.descriptions)
		
		
		row = '%s\t%s\t%g\t%s\t%s\t\n' % (entry.alignments[0].hit_def, entry.alignments[0].hit_id, besthit.e, entry.query_id, entry.alignments[0].length) 

		f_out.write(row)

		print row
		