from Bio import Blast
from Bio.Blast import Applications
from Bio import SeqIO


trinity_fasta = open("Trinity.fasta", "rU")
top20_fasta = open("Top20Trinity.fasta","w")

maxentries = 20
e=1
topentries=[]
for record in SeqIO.parse(trinity_fasta, "fasta") :
	if  e > maxentries:
		break
	
	e=e+1
	topentries.append(record)


SeqIO.write(topentries,top20_fasta,"fasta")
trinity_fasta.close()