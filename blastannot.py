from Bio import Blast
from Bio.Blast.Applications import NcbiblastxCommandline
from Bio import SeqIO
import tempfile
import os

top20_fasta = open("Top20Trinity.fasta","r")

database_path = "/Users/brettshiel/Desktop/nr/nr"

for record in SeqIO.parse(top20_fasta, "fasta") :
	query_file, query_file_name = tempfile.mkstemp()
	SeqIO.write(record,query_file_name,"fasta")
	cline = NcbiblastxCommandline(query=query_file_name, db="nr",evalue=0.001, 
		remote=True, ungapped=False,show_gis=True,outfmt=5)
	print cline
#	os.remove(query_file_name)

top20_fasta.close()